FROM registry.gitlab.com/thai-med-friend/docker-php7.4-fpm:latest

RUN apk add --update --no-cache nginx curl supervisor

# avoid error, nginx: [emerg] open() "/run/nginx/nginx.pid" failed (2: No such file or directory)
RUN mkdir -p /run/nginx

COPY nginx.conf /etc/nginx/nginx.conf

# Configure supervisord
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf



# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

RUN apk add --update nodejs
RUN apk add yarn
RUN apk add bash
RUN apk add ffmpeg
RUN apk add shellcheck